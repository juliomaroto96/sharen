package com.sharen.domain.repository;

import com.sharen.domain.model.Group;
import com.sharen.domain.model.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends JpaRepository<Member, Long> {
    Member findByUserId(Long id);
    Member findByUserIdAndGroupId(Long userId, Long groupId);
    Member findByUserIdAndProjectId(Long userId, Long projectId);
}
