package com.sharen.domain.repository;

import com.sharen.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    User findById(long id);
    User findByUserName(String userName);
    User findByEmailAddressAndPassword(String emailAddress, String password);
    User findByEmailAddress(String emailAddress);
}
