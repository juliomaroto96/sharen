package com.sharen.domain.repository;

import com.sharen.domain.model.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    Comment findById(Long id);
    Page<Comment> findByPublicationId(Long id, Pageable page);
}
