package com.sharen.domain.repository;

import com.sharen.domain.model.Publication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PublicationRepository extends JpaRepository<Publication, Long> {
    Publication findById(long id);
    List<Publication> findByProjectId (long id);
    Page<Publication> findByProjectId(long id, Pageable page);
    Page<Publication> findByGroupId(long id, Pageable page);
}
