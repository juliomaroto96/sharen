package com.sharen.domain.repository;

import com.sharen.domain.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {
    Vote findByMemberId(Long id);
    Vote findByMemberIdAndCommentId(Long memberId, Long commentId);
}
