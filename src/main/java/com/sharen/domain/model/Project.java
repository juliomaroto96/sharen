package com.sharen.domain.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.omg.CORBA.ServerRequest;
import org.springframework.web.bind.annotation.RequestAttribute;

import javax.persistence.*;
import java.util.List;

@Entity
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String projectName;
    private String twitterSite;
    private String facebookSite;
    private String website;
    private String description;
    private String imagePathName;

    private Integer groupsNumber;
    private Integer membersNumber;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Publication> publications;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Group> groups;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Member> members;

    public Project() {

    }

    public Project(
            String projectName,
            String twitterSite,
            String facebookSite,
            String website,
            String description,
            String imagePathName
    )
    {
        this.projectName = projectName;
        this.twitterSite = twitterSite;
        this.facebookSite = facebookSite;
        this.website = website;
        this.description = description;
        this.imagePathName = imagePathName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagePathName() {
        return imagePathName;
    }

    public void setImagePathName(String imagePathName) {
        this.imagePathName = imagePathName;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<Publication> getPublications() {
        return publications;
    }

    public void setPublications(List<Publication> publication) {
        this.publications = publication;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTwitterSite() {
        return twitterSite;
    }

    public void setTwitterSite(String twitterSite) {
        this.twitterSite = twitterSite;
    }

    public String getFacebookSite() {
        return facebookSite;
    }

    public void setFacebookSite(String facebookSite) {
        this.facebookSite = facebookSite;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Integer getGroupsNumber() {
        return groupsNumber;
    }

    public void setGroupsNumber(Integer groupsNumber) {
        this.groupsNumber = groupsNumber;
    }

    public Integer getMembersNumber() {
        return membersNumber;
    }

    public void setMembersNumber(Integer membersNumber) {
        this.membersNumber = membersNumber;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }
}
