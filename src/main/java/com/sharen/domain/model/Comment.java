package com.sharen.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonAppend;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Publication publication;

    @ManyToOne
    private Member member;

    private Long parent;

    @Transient
    private Boolean userHasUpvoted;

    @Column(length = 355)
    private String content;

    private Date created;

    @Column(name = "upvote_count")
    @JsonProperty("upvote_count")
    private Integer upvoteCount = 0;

    private String fullname;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Vote> votes;

    public Comment() {
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getFullname() {
        return fullname;
    }

    public Integer getUpvoteCount() {
        return upvoteCount;
    }

    public void setUpvoteCount(Integer upvoteCount) {
        this.upvoteCount = upvoteCount;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }

    public Boolean getUserHasUpvoted() {
        return userHasUpvoted;
    }

    public void setUserHasUpvoted(Boolean userHasUpvoted) {
        this.userHasUpvoted = userHasUpvoted;
    }
}
