package com.sharen.domain.model.DTOs;

/**
 * Created by julio on 11/6/17.
 */
public class MemberDTO {
    private String name;
    private String nickname;
    private boolean isMember;

    public MemberDTO() {

    }

    public MemberDTO(String nickname, String name, boolean isMember) {
        this.nickname = nickname;
        this.name = name;
        this.isMember = isMember;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public boolean isMember() {
        return isMember;
    }

    public void setMember(boolean member) {
        isMember = member;
    }
}
