package com.sharen.domain.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private Boolean deleted = false;
    @Column(nullable = false)
    private Boolean admin = false;

    private String userName;
    private String name;
    private String emailAddress;
    private String facebookToken;
    private String twitterToken;
    private String password;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Member> members;

    public User() {

    }

    public User(String userName){
        this.userName = userName;
    }

    public User(String userName, String name, String emailAddress, String password, Boolean admin) {
        this.userName = userName;
        this.name = name;
        this.emailAddress = emailAddress;
        this.password = password;
        this.admin = admin;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getFacebookToken() {
        return facebookToken;
    }

    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    public String getTwitterToken() {
        return twitterToken;
    }

    public void setTwitterToken(String twitterToken) {
        this.twitterToken = twitterToken;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void toSecureFormat() {
        this.deleted = null;
        this.admin = null;
        this.facebookToken = null;
        this.twitterToken = null;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return "User {" +
                "id=" + id +
                ", deleted=" + deleted +
                ", admin=" + admin +
                ", username='" + userName + '\'' +
                ", name='" + name + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", facebookToken='" + facebookToken + '\'' +
                ", twitterToken='" + twitterToken + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
