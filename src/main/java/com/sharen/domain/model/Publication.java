package com.sharen.domain.model;

import com.sharen.domain.model.Group;
import com.sharen.domain.model.Project;

import javax.persistence.*;
import java.util.List;

@Entity
public class Publication {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String title;

    @Column(length = 2555)
    private String message;

    private long fb_like;
    private long fb_share;
    private long tw_share;

    @ManyToOne
    private Project project;

    @ManyToOne
    private Group group;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Comment> comments;

    public Publication() {

    }

    public Publication(String title, String message) {
        this.title = title;
        this.message = message;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getFb_like() {
        return fb_like;
    }

    public void setFb_like(long fb_like) {
        this.fb_like = fb_like;
    }

    public long getFb_share() {
        return fb_share;
    }

    public void setFb_share(long fb_share) {
        this.fb_share = fb_share;
    }

    public long getTw_share() {
        return tw_share;
    }

    public void setTw_share(long tw_share) {
        this.tw_share = tw_share;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
