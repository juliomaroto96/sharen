package com.sharen.session;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserPreferences {
    private Long userId;
    private Boolean isLogged = false;
    private String userName;
    private String lastRequestPage;
    private Long nowProjectId;
    private Long nowGroupId;

    public Boolean getLogged() {
        return isLogged;
    }

    public void setLogged(Boolean logged) {
        isLogged = logged;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastRequestPage() {
        return lastRequestPage;
    }

    public void setLastRequestPage(String lastRequestPage) {
        this.lastRequestPage = lastRequestPage;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getNowProjectId() {
        return nowProjectId;
    }

    public void setNowProjectId(Long nowProjectId) {
        this.nowProjectId = nowProjectId;
    }

    public Long getNowGroupId() {
        return nowGroupId;
    }

    public void setNowGroupId(Long nowGroupId) {
        this.nowGroupId = nowGroupId;
    }
}
