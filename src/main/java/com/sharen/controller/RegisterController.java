package com.sharen.controller;

import com.sharen.domain.model.User;
import com.sharen.domain.repository.UserRepository;
import com.sharen.session.UserPreferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RegisterController {

    @Autowired
    UserPreferences userPreferences;

    @Autowired
    UserRepository userRepository;

    @RequestMapping("/register")
    public String register(Model model) {
        if (userPreferences.getLogged()) {
            model.addAttribute("logged", true);
        }

        return "register";
    }

    @RequestMapping("/register/submit")
    public ResponseEntity<User> submit(@RequestBody User user) {

        ResponseEntity<User> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if (
                user.getEmailAddress() != null &&
                        user.getName() != null &&
                        user.getPassword() != null
                ) {
            User resultUser = userRepository.findByEmailAddress(user.getEmailAddress());

            if (resultUser == null) {
                response = new ResponseEntity<User>(userRepository.save(user), HttpStatus.OK);
            } else {
                response = new ResponseEntity<User>(HttpStatus.NOT_FOUND);
            }

        } else {
            response = new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }

        return response;
    }
}
