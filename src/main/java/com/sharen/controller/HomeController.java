package com.sharen.controller;

import com.sharen.session.UserPreferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
    @Autowired
    private UserPreferences userPreferences;

    @RequestMapping("/")
    public String home(Model model) {

        if (userPreferences.getLogged()) {
            model.addAttribute("logged", true);
        }

        return "home";
    }
}
