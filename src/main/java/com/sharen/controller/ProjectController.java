package com.sharen.controller;

import com.sharen.domain.model.Group;
import com.sharen.domain.model.Project;
import com.sharen.domain.model.Publication;
import com.sharen.domain.repository.GroupRepository;
import com.sharen.domain.repository.ProjectRepository;
import com.sharen.domain.repository.PublicationRepository;
import com.sharen.session.UserPreferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.*;

@Controller
public class ProjectController {

    @Autowired
    UserPreferences userPreferences;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    PublicationRepository publicationRepository;

    @RequestMapping("/projects")
    public String projects(Map<String, Object> model) {
        if (userPreferences.getLogged()) {
            Page<Project> projects = projectRepository.findAll(new PageRequest(0, 12));

            List<Project> projectList = projects.getContent();
            List<List<Project>> projectsArrayList = new ArrayList<>();

            Integer i = 0;
            Integer rowCounter = 0;
            final Integer VARIATION = 4;

            while (i <= projectList.size() && rowCounter < 4) {
                Integer maxOfSublist = i + VARIATION;

                while (maxOfSublist > projectList.size()) {
                    maxOfSublist--;
                }

                List<Project> temp = projectList.subList(i, maxOfSublist);

                projectsArrayList.add(temp);

                i += VARIATION;
                rowCounter++;
            }

            model.put("logged", true);
            model.put("projects", projectsArrayList) ;
        }

        return "projects";
    }

    @RequestMapping("/projects/create")
    public String createProject(Model model) {

        if (userPreferences.getLogged()) {
            model.addAttribute("logged", true);
        }

        return "create_project";
    }

    @RequestMapping("/projects/update/{id}")
    public String updateProject(Model model, @PathVariable int id) {

        if (userPreferences.getLogged()) {
            userPreferences.setLastRequestPage("/projects/update/" + id);
            model.addAttribute("logged", true);

            Project project  = projectRepository.findById((long) id);

            List<Group> groupList = groupRepository.findByProjectId((long) id);
            project.setGroups(groupList);

            Page<Publication> publicationList = publicationRepository.findByProjectId((long) id, new PageRequest(0, 6));
            project.setPublications(publicationList.getContent().subList(0, publicationList.getContent().size()));

            if (project.getId() == id) {
                model.addAttribute("project", project);
            }
        }

        return "update_project";
    }
}
