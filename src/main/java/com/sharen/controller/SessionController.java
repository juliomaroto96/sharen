package com.sharen.controller;

import com.sharen.domain.model.User;
import com.sharen.domain.repository.UserRepository;
import com.sharen.session.UserPreferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/session")

public class SessionController {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserPreferences userPreferences;

    @RequestMapping(value = "/login/submit", method = RequestMethod.POST)
    public ResponseEntity<User> getUser(@RequestBody User user) {

        ResponseEntity<User> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if (user.getEmailAddress() != null) {
            User resultUser = repository.findByEmailAddressAndPassword(user.getEmailAddress(), user.getPassword());

            if (resultUser != null) {
                userPreferences.setUserName(resultUser.getUserName());
                userPreferences.setLogged(true);
                userPreferences.setUserId(resultUser.getId());

                resultUser.toSecureFormat();
                response = new ResponseEntity<>(resultUser, HttpStatus.OK);
            }
        }
        return response;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public Boolean logout() {
        userPreferences.setUserName("");
        userPreferences.setLogged(false);

        return true;
    }
}
