package com.sharen.controller;

import com.sharen.domain.model.Group;
import com.sharen.domain.model.Publication;
import com.sharen.domain.repository.GroupRepository;
import com.sharen.domain.repository.PublicationRepository;
import com.sharen.session.UserPreferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class GroupController {

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    PublicationRepository publicationRepository;

    @Autowired
    UserPreferences userPreferences;

    @RequestMapping("/group/{id}")
    public String readGroup(@PathVariable long id, Model model) {

        if (userPreferences.getLogged()) {

            model.addAttribute("logged", true);

            Group resGroup = groupRepository.findById(id);
            Page<Publication> publicationList = publicationRepository.findByGroupId(id, new PageRequest(0, 6));

            resGroup.setPublications(publicationList.getContent().subList(0, publicationList.getContent().size()));

            model.addAttribute("group", resGroup);
        }

        return "read_group";
    }
}
