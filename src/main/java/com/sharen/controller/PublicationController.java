package com.sharen.controller;

import com.sharen.domain.model.Publication;
import com.sharen.domain.repository.GroupRepository;
import com.sharen.domain.repository.ProjectRepository;
import com.sharen.domain.repository.PublicationRepository;
import com.sharen.session.UserPreferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;

@Controller
public class PublicationController {

    @Autowired
    PublicationRepository publicationRepository;

    @Autowired
    UserPreferences userPreferences;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    GroupRepository groupRepository;

    @RequestMapping("/publication/{publicationId}")
    public String getPublication(@PathVariable Long publicationId, Model model) {

        if (userPreferences.getLogged()) {
            model.addAttribute("logged", true);

            Publication resPublication = publicationRepository.findById(publicationId);

            if (resPublication != null) {
                model.addAttribute("publication", resPublication);
            }
        }
        return "read_publication";
    }

    @RequestMapping("/publication/create")
    public String createPublication(Model model) {

        if (userPreferences.getLogged()) {
            model.addAttribute("logged", true);
        }

        return "create_publication";
    }

    @PostMapping(value = "/publication/create/submit")
    public ModelAndView submitPublication(
            HttpServletResponse httpServletResponse,
            @RequestParam("post_title") String postTitle,
            @RequestParam("post_message") String postMessage
    ) {

        StringBuilder projectUrl = new StringBuilder("/publication/");

        Publication publication = new Publication(postTitle, postMessage);

        if (userPreferences.getNowProjectId() != null) {
            publication.setProject(projectRepository.findById(userPreferences.getNowProjectId()));
        } else {
            publication.setGroup(groupRepository.findById(userPreferences.getNowGroupId()));
        }

        userPreferences.setNowProjectId(null);
        userPreferences.setNowGroupId(null);

        projectUrl.append(publicationRepository.save(publication).getId());

        return new ModelAndView("redirect:" + projectUrl.toString());
    }

}
