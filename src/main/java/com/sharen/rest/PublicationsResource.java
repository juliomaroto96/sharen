package com.sharen.rest;

import com.sharen.domain.model.Publication;
import com.sharen.domain.repository.PublicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
public class PublicationsResource {

    @Autowired
    PublicationRepository publicationRepository;

    @RequestMapping(
            value = "/publications",
            method = RequestMethod.GET)
    public Page<Publication> getPublications(
            @RequestParam(value = "project", required = false) Long projectId,
            @RequestParam(value = "group", required = false) Long groupId,
            @RequestParam(value = "page") int page,
            @RequestParam(value = "size") int size) {

        Page<Publication> response = null;

        if (projectId != null) {
            response = publicationRepository.findByProjectId(projectId, new PageRequest(page, size));
        } else if (groupId != null) {
            response = publicationRepository.findByGroupId(groupId, new PageRequest(page, size));
        }

        return response;
    }
}
