package com.sharen.rest;

import com.sharen.domain.model.*;
import com.sharen.domain.repository.*;
import com.sharen.session.UserPreferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CommentsResource {
    @Autowired
    CommentRepository commentRepository;

    @Autowired
    PublicationRepository publicationRepository;

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    UserPreferences userPreferences;

    @Autowired
    VoteRepository voteRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    ProjectRepository projectRepository;

    @RequestMapping("/comments")
    public List<Comment> getComments(@RequestParam(value = "post") Long publicationId, Pageable page) {

        List<Comment> commentList = null;

        if (userPreferences.getLogged()) {
            Page<Comment> commentPage = commentRepository.findByPublicationId(publicationId, page);

            commentList = commentPage.getContent().subList(0, commentPage.getContent().size());

            Publication resPublication = publicationRepository.findById(publicationId);

            Group publicationGroup = resPublication.getGroup();
            Group resGroup = null;

            if (publicationGroup != null) {
                resGroup = groupRepository.findById(publicationGroup.getId());
            }

            Project publicationProject = resPublication.getProject();
            Project resProject = null;

            if (publicationProject != null) {
                resProject = projectRepository.findById(resPublication.getProject().getId());
            }

            Member nowMember = null;

            if (resGroup != null) {
                nowMember = memberRepository.findByUserIdAndGroupId(userPreferences.getUserId(), resGroup.getId());
            } else {
                nowMember = memberRepository.findByUserIdAndProjectId(userPreferences.getUserId(), resProject.getId());
            }

            for (Comment aCommentList : commentList) {

                Vote resVote = voteRepository.findByMemberIdAndCommentId(nowMember.getId(), aCommentList.getId());

                if (resVote != null) {
                    aCommentList.setUserHasUpvoted(true);
                } else {
                    aCommentList.setUserHasUpvoted(false);
                }
            }
        }

        return commentList;
    }

    @RequestMapping("/comments/create")
    public Comment setComment(@RequestParam(value = "post") Long publicationId, @RequestBody Comment comment) {

        Comment resComment = null;

        comment.setPublication(publicationRepository.findById(publicationId));

        if (comment.getContent() != null) {
            comment.setFullname(userPreferences.getUserName());
            comment.setMember(memberRepository.findByUserId(userPreferences.getUserId()));
            resComment = commentRepository.save(comment);
        }

        return resComment;
    }

    @RequestMapping("/comments/{commentId}/upvotes/")
    public Comment upVote(@PathVariable Long commentId, @RequestBody Comment comment) {

        Comment updatedComment = null;

        if (userPreferences.getLogged()) {
            Member nowMember = memberRepository.findByUserId(userPreferences.getUserId());
            Vote resVote = voteRepository.findByMemberIdAndCommentId(nowMember.getId(), commentId);

            if (resVote == null) {
                Vote newVote = new Vote();

                Comment resComment = commentRepository.findById(commentId);

                newVote.setComment(resComment);
                newVote.setMember(nowMember);
                resComment.setUpvoteCount(resComment.getUpvoteCount() + 1);

                voteRepository.save(newVote);
                commentRepository.save(resComment);

                updatedComment = resComment;
            } else {
                updatedComment = comment;
            }
        }

        return updatedComment;
    }
}
