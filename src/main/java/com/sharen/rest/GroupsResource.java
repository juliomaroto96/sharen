package com.sharen.rest;


import com.sharen.domain.model.Group;
import com.sharen.domain.model.Project;
import com.sharen.domain.repository.GroupRepository;
import com.sharen.domain.repository.ProjectRepository;
import com.sharen.session.UserPreferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GroupsResource {

    @Autowired
    UserPreferences userPreferences;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    GroupRepository groupRepository;

    @RequestMapping(value = "/groups/create", method = RequestMethod.POST)
    public ResponseEntity<Group> createGroup(@RequestBody Group group) {

        ResponseEntity<Group> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if (
                group.getName() != null &&
                group.getDescription() != null
                ) {

            String[] splittedLastRequestPage = userPreferences.getLastRequestPage().split("/");

            String projectId = splittedLastRequestPage[splittedLastRequestPage.length - 1];

            Project resultProject = projectRepository.findById(Long.parseLong(projectId));

            if (resultProject != null) {

                group.setProject(resultProject);

                Group createdGroup = groupRepository.save(group);

                response = new ResponseEntity<>(createdGroup, HttpStatus.OK);
            }

        }

        return response;
    }
}
