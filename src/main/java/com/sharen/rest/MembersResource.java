package com.sharen.rest;

import com.sharen.domain.model.*;
import com.sharen.domain.repository.MemberRepository;
import com.sharen.domain.repository.ProjectRepository;
import com.sharen.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.sharen.domain.model.DTOs.MemberDTO;

@RestController
public class MembersResource {

    @Autowired
    UserRepository userRepository;

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    ProjectRepository projectRepository;

    @RequestMapping(value = "/member", method = RequestMethod.GET)

    public ResponseEntity<MemberDTO> getMember(@RequestParam("nickname") String username) {
        ResponseEntity<MemberDTO> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        User user = userRepository.findByUserName(username);
        Member member = memberRepository.findByUserId(user.getId());

        if (user != null) {

            MemberDTO memberDTO = new MemberDTO(user.getUserName(), user.getName(), false);

            if (member != null) {
                memberDTO = new MemberDTO(user.getUserName(), user.getName(), true);
            }

            response = new ResponseEntity<>(memberDTO, HttpStatus.OK);
        }

        return response;
    }

    @RequestMapping(value = "/member/set", method = RequestMethod.GET)

    public ResponseEntity<MemberDTO> setProjectMember
            (

                    @RequestParam("nickname") String username,
                    @RequestParam("project") Long projectId

            ) {
        ResponseEntity<MemberDTO> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        User user = userRepository.findByUserName(username);

        Member member =
                new Member(
                        projectId,
                        false,
                        new Role(0),
                        user,
                        projectRepository.findById(projectId),
                        new Group(0)
                        );

        Member createdMember = memberRepository.save(member);

        if (createdMember != null) {
            MemberDTO memberDTO = new MemberDTO(user.getUserName(), user.getName(), true);

            response = new ResponseEntity<>(memberDTO, HttpStatus.OK);
        }

        return response;
    }
}
