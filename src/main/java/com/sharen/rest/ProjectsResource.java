package com.sharen.rest;

import com.sharen.domain.model.Project;
import com.sharen.domain.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProjectsResource {

    @Autowired
    ProjectRepository projectRepository;

    @RequestMapping(value = "/projects/", method = RequestMethod.GET)
    public Page<Project> projects(Pageable page) {
        return projectRepository.findAll(page);
    }

    @RequestMapping(value = "/projects/create/submit", method = RequestMethod.POST)
    public ResponseEntity<Project> setProject(@RequestBody Project project) {
        ResponseEntity<Project> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if (
                project.getProjectName() != null &&
                        project.getImagePathName() != null &&
                        project.getDescription() != null &&
                        project.getGroupsNumber() != null &&
                        project.getMembersNumber() != null
                ) {
            Project projectResult = projectRepository.findByProjectName(project.getProjectName());

            if (projectResult == null) {
                Project savedProject = projectRepository.save(project);
                response = new ResponseEntity<>(savedProject, HttpStatus.OK);
            }
        }

        return response;
    }
}
