package com.sharen;

import com.sharen.domain.model.Group;
import com.sharen.domain.model.Project;
import com.sharen.domain.model.Publication;
import com.sharen.domain.model.User;
import com.sharen.domain.repository.GroupRepository;
import com.sharen.domain.repository.ProjectRepository;
import com.sharen.domain.repository.PublicationRepository;
import com.sharen.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EntityScan(basePackages = {"com.sharen.domain"})
@EnableJpaRepositories(basePackages = {"com.sharen.domain"})
public class Application {

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	UserRepository repository;

	@Autowired
	GroupRepository groupRepository;

	@Autowired
	PublicationRepository publicationRepository;

	/*
	@PostConstruct
	public void init() {

		User user = new User("chappie", "chappie rodriguez", "darksyders@gmail.com", "1234", true);

		repository.save(user);

		Project project = new Project(
				"sharen", "www.twitter.com/sharen",
				"wwww.facebook.com/sharen",
				"www.sharen.com",
				"A sharen project",
				"http://www.comunicacionesua.cl/wp-content/uploads/2016/10/Examen_Prueba_Enlace-2.jpg"
		);

		Project project2 = new Project(
				"nike",
				"www.twitter.com/nike",
				"wwww.facebook.com/nike",
				"www.nike.com",
				"Nike sport brand",
				"http://socialgeek.co/wp-content/uploads/2013/12/Logo-Nike.jpg"
		);

		Project project3 = new Project(
				"coca-cola",
				"www.twitter.com/coca_cola",
				"wwww.facebook.com/coca_cola",
				"www.coca-cola.com",
				"A drink of Coca-Cola Company LTD",
				"https://www.cocacola.es/content/dam/GO/CokeZone/Spain/Coca-Cola%20Magician/800x654-Coca-cola-Zero-2.jpg"
		);

		Project project4 = new Project(
				"Xiaomi",
				"www.twitter.com/xiaomi",
				"wwww.facebook.com/xiaomi",
				"www.xiaomi.com",
				"The mobility in your hands",
				"https://elandroidelibre.elespanol.com/wp-content/uploads/2016/02/xiaomi.png"
		);

		Project project5 = new Project(
				"Pepsi", "www.twitter.com/pepsi",
				"wwww.facebook.com/pepsi",
				"www.pepsico.com",
				"Makes your hart fall in love",
				"https://pbs.twimg.com/profile_images/762773014010327040/q4Bc_-Sz.jpg"
		);

		Project project6 = new Project(
				"MySQL",
				"www.twitter.com/mysql",
				"wwww.facebook.com/mysql",
				"www.oracle.com/mysql",
				"Mastering SGDBs",
				"https://4.bp.blogspot.com/-rkkvehCgbZs/WFyfY2qn8OI/AAAAAAAAxrc/oM_QfZgAzeMQJnybaEngWYIZpOuHGbN4gCLcB/s1600/MySQL.svg.png"
		);

		Project project7 = new Project(
				"Nokia",
				"www.twitter.com/nokia",
				"wwww.facebook.com/nokia",
				"www.nokia.com",
				"Connecting people",
				"http://www.masiya.net/images/nokia_logo_0.png"
		);

		Project project8 = new Project(
				"LG",
				"www.twitter.com/lg",
				"wwww.facebook.com/lg",
				"www.lg.com",
				"Life's good",
				"https://s3.amazonaws.com/poderpda/2015/04/LG-logo.jpg"
		);

		Project project9 = new Project(
				"Samsung",
				"www.twitter.com/samsung",
				"wwww.facebook.com/samsung",
				"www.samsung.com",
				"Let it fly",
				"http://hdguru3d.com/wp-content/uploads/2013/01/samsung_logo.jpg"
		);

		Project project10 = new Project(
				"Adidas",
				"www.twitter.com/adidas",
				"wwww.facebook.com/adidas",
				"www.adidas.com",
				"Adidas sport brand",
				"http://www.brandemia.org/wp-content/uploads/2012/05/adidas_logo_original.jpg"
		);
		Project project11 = new Project(
				"Zara",
				"www.twitter.com/zara",
				"wwww.facebook.com/zara",
				"www.zara.com",
				"Wear it",
				"http://res.cloudinary.com/thefader/image/upload/zara_vnaem4.jpg"
		);
		Project project12 = new Project(
				"Mango",
				"www.twitter.com/mango",
				"wwww.facebook.com/mango",
				"www.mango.com",
				"Love your imperfections",
				"http://www.brandemia.org/wp-content/uploads/2011/05/mang_port.jpg"
		);



		projectRepository.save(project);
		projectRepository.save(project2);
		projectRepository.save(project3);
		projectRepository.save(project4);
		projectRepository.save(project5);
		projectRepository.save(project6);
		projectRepository.save(project7);
		projectRepository.save(project8);
		projectRepository.save(project9);
		projectRepository.save(project10);
		projectRepository.save(project11);
		projectRepository.save(project12);

		Group group = new Group("Diseño", "Este equipo se encarga de diseñar la UI/UX");
		Group group2 = new Group("Desarrollo", "Equipo de programadores de Sharen");
		Group group3 = new Group("Ventas", "Equipo de ventas del proyecto.");

		group.setProject(project);
		group2.setProject(project);
		group3.setProject(project);

		groupRepository.save(group);
		groupRepository.save(group2);
		groupRepository.save(group3);

		Publication publication1 = new Publication("Sharen has born 1", "Sharen is a new idea for easing networking");
		Publication publication2 = new Publication("Sharen has born 2", "Sharen is a new idea for easing networking");
		Publication publication3 = new Publication("Sharen has born 3", "Sharen is a new idea for easing networking");
		Publication publication4 = new Publication("Sharen has born 4", "Sharen is a new idea for easing networking");
		Publication publication5 = new Publication("Sharen has born 5", "Sharen is a new idea for easing networking");
		Publication publication6 = new Publication("Sharen has born 6", "Sharen is a new idea for easing networking");
		Publication publication7 = new Publication("Sharen has born 7", "Sharen is a new idea for easing networking");
		Publication publication8 = new Publication("Sharen has born 8", "Sharen is a new idea for easing networking");
		Publication publication9 = new Publication("Sharen has born 9", "Sharen is a new idea for easing networking");
		Publication publication10 = new Publication("Sharen has born 10", "Sharen is a new idea for easing networking");
		Publication publication11 = new Publication("Sharen has born 11", "Sharen is a new idea for easing networking");
		Publication publication12 = new Publication("Sharen has born 12", "Sharen is a new idea for easing networking");

		publication1.setProject(project);
		publication2.setProject(project);
		publication3.setProject(project);
		publication4.setProject(project);
		publication5.setProject(project);
		publication6.setProject(project);
		publication7.setProject(project);
		publication8.setProject(project);
		publication9.setProject(project);
		publication10.setProject(project);
		publication11.setProject(project);
		publication12.setProject(project);

		publicationRepository.save(publication1);
		publicationRepository.save(publication2);
		publicationRepository.save(publication3);
		publicationRepository.save(publication4);
		publicationRepository.save(publication5);
		publicationRepository.save(publication6);
		publicationRepository.save(publication7);
		publicationRepository.save(publication8);
		publicationRepository.save(publication9);
		publicationRepository.save(publication10);
		publicationRepository.save(publication11);
		publicationRepository.save(publication12);

	}
	*/

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
