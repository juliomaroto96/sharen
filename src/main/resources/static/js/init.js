var pageCounter = 1;
var pagePublicationsCounter = 0;
var firstPageBack = true;

(function ($) {
    $(function () {

        $('.button-collapse').sideNav();
        $('.tooltipped').tooltip({delay: 50});

        $('.collapsible')
            .collapsible({
                accordion: false,
                onOpen: function (el) {
                },
                onClose: function (el) {
                }
            });

        $('.modal').modal({
                dismissible: true,
                opacity: .5,
                inDuration: 300,
                outDuration: 200,
                startingTop: '4%',
                endingTop: '10%',
                ready: function (modal, trigger) {
                },
                complete: function () {
                }
            }
        );

        $('#comments-container').comments({
            profilePictureURL: 'https://app.viima.com/static/media/user_profiles/user-icon.png',
            getComments: function (success, error) {
                $.ajax({
                    type: 'get',
                    url: '/comments?post=' + $('#publication-id').val(),
                    success: function (res) {
                        console.log(res);
                        success(res);
                    },
                    error: error
                })
            },
            postComment: function (commentJSON, success, error) {
                commentJSON.id = 0;
                commentJSON.parent = parseInt(commentJSON.parent);

                $.ajax({
                    type: 'post',
                    contentType: "application/json",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    url: '/comments/create?post=' + $('#publication-id').val(),
                    data: JSON.stringify(commentJSON),
                    success: function (comment) {
                        success(comment);
                    },
                    error: error
                });
            },
            upvoteComment: function (commentJSON, success, error) {
                var commentURL = '/comments/' + commentJSON.id;
                var upvotesURL = commentURL + '/upvotes/';

                if (commentJSON.user_has_upvoted) {
                    $.ajax({
                        contentType: "application/json",
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        type: 'post',
                        url: upvotesURL,
                        data: JSON.stringify(commentJSON),
                        success: function (res) {
                            console.log(commentJSON);
                            success(commentJSON);
                        },
                        error: error
                    });
                } else {
                    $.ajax({
                        type: 'delete',
                        url: upvotesURL,
                        success: function () {
                            success(commentJSON)
                        },
                        error: error
                    });
                }
            }
        });

        $('#froala_editor').froalaEditor({toolbarInline: false});


        showLoginForm();
        handleEvents();
    });
})(jQuery);

function handleEvents() {
    $('#login-btn').on('click', function (evt) {
        evt.preventDefault();

        var enteredUserName = $('#email').val();
        var enteredPassword = $('#password').val();

        console.log(enteredUserName + ', ' + enteredPassword);

        login(enteredUserName, enteredPassword);
    });

    $('#submit-login-btn').on('click', function () {

        var enteredUserName = $('#email_login2').val();
        var enteredPassword = $('#password_login2').val();

        login(enteredUserName, enteredPassword);
    });

    $('.logout').on('click', closeSessionHandler);

    $(window).scroll(function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            loadMoreProjects();
        }
    });

    $('#watch-more-btn').on('click', loadMoreProjects);

    $('#register-btn').on('click', function (evt) {
        evt.preventDefault();
        window.location.replace("/register");
    });

    $('#register-submit-btn').on('click', function (evt) {
        evt.preventDefault();
        registerHandler();
    });

    $('#add-project-btn').on('click', function () {
        window.location.replace("/projects/create");
    });

    $('#create-project-btn').on('click', function (evt) {
        evt.preventDefault();
        handleProjectCreation();
    });

    $('#create-group-submit-btn').on('click', function () {
        handleGroupCreation();
    });

    $("#next_project_post").on('click', function (evt) {

        if (pagePublicationsCounter >= 0) {
            pagePublicationsCounter++;

            $.ajax({
                url: '/publications/?project=' + $('#project-id').val() + '&page=' + pagePublicationsCounter + '&size=6',
                method: 'get',
                success: handleGettingPublications,
                error: handleGettingPublicationsError
            });
        }

    });

    $("#prev_project_post").on('click', function () {

        if (pagePublicationsCounter >= 1) {
            pagePublicationsCounter--;

            $.ajax({
                url: '/publications/?project=' + $('#project-id').val() + '&page=' + pagePublicationsCounter + '&size=6',
                method: 'get',
                success: handleGettingLessPublications,
                error: handleGettingLessPublicationsError
            });
        } else {
            $("i:contains('chevron_left')")
                .parent()
                .parent()
                .addClass('disabled');
        }
    });

    $("#next_group_post").on('click', function (evt) {

        if (pagePublicationsCounter >= 0) {
            pagePublicationsCounter++;

            $.ajax({
                url: '/publications/?group=' + $('#group-id').val() + '&page=' + pagePublicationsCounter + '&size=6',
                method: 'get',
                success: handleGettingPublications,
                error: handleGettingPublicationsError
            });
        }

    });

    $("#prev_group_post").on('click', function () {

        if (pagePublicationsCounter >= 1) {
            pagePublicationsCounter--;

            $.ajax({
                url: '/publications/?group=' + $('#group-id').val() + '&page=' + pagePublicationsCounter + '&size=6',
                method: 'get',
                success: handleGettingLessPublications,
                error: handleGettingLessPublicationsError
            });
        } else {
            $("i:contains('chevron_left')")
                .parent()
                .parent()
                .addClass('disabled');
        }
    });
    
    $("#search-icon").on('click', function () {
        if ($("#search").val().length > 0) {
            $.ajax({
                url: '/member?nickname=' + $("#search").val(),
                method: 'get',
                success: handleGettingNickname,
                error: handleGettingNicknameError
            });
        }
    });

    $('#add-del-btn').on('click', function (evt) {
        if ($(this).attr("data-badge-caption") == "Agregar") {
            console.log("HOLA MUNDO")
        }
    });
}

function closeSessionHandler() {
    closeBackEndSession();
}

function closeBackEndSession() {
    $.ajax({
        url: "/session/logout",
        type: 'get',
        success: function () {
            showLoginBtn(function () {
                showLoginForm();
            });
            window.location.replace("/");
        }
    });
}

function showLoginBtn(callback) {
    $('#exit').hide();

    if ($("#login").length) {
        $(this).show();
    } else {
        $('#proyectos')
            .parent()
            .parent()
            .prepend(
                "<ul class='right hide-on-med-and-down'>" +
                "<li>" +
                "<a href='#' id='login'>Entrar</a>" +
                "</li>" +
                "</ul>"
            );
    }

    if (typeof callback === "function") {
        callback();
    }
}


function login(enteredUserName, enteredPassword) {

    var user = {
        emailAddress: enteredUserName,
        password: enteredPassword
    };

    $.ajax({
        url: "/session/login/submit",
        contentType: "application/json",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        type: 'post',
        data: JSON.stringify(user),
        success: function (result) {
            handleLoginRequest(user, result);
        },
        error: function () {
            if ($('#bad-login-msg').length) {
                $(this).show();
            } else {
                $("#password")
                    .parent()
                    .append(
                        "<span id='bad-login-msg' style='text-align: center'>" +
                        "Datos de acceso incorrectos" +
                        "</span>" +
                        "<br>"
                    );
            }
        }
    });

    $('#exit').bind('click', closeSessionHandler);
}

function handleLoginRequest(user, result) {
    if (
        result.emailAddress == user.emailAddress &&
        result.password == user.password
    ) {
        hideLoginBtn();
    }
}

function hideLoginBtn() {
    var loginBtn = $('#login');

    loginBtn
        .webuiPopover('hide');
    loginBtn
        .hide();

    if ($('#exit').length) {
        $(this).show();
    } else {
        $('#proyectos')
            .parent()
            .parent()
            .prepend("" +
                "<ul class='right hide-on-med-and-down'>" +
                "<li>" +
                "<a href='#' id='exit'>Salir</a>" +
                "</li>" +
                "</ul>");
    }

    $('#bad-login-msg').hide();

    handleEvents();
    window.location.replace("/projects");
}

function showLoginForm() {
    $('#login').webuiPopover({
        url: '#login-form',
        width: 300
    });
}

function loadMoreProjects() {
    $.ajax({
        url: '/projects/?page=' + pageCounter + '&size=12',
        type: 'get',
        success: handleLoadOfProjects,
        error: handleErrorLoadOfProjects
    });

    pageCounter++;
}

function handleErrorLoadOfProjects() {
    console.log("error");
}

function handleLoadOfProjects(result) {
    if (result.numberOfElements > 0) {
        var projectCounter = 0;

        var columnsLength = result.numberOfElements;

        if (columnsLength > 0) {
            for (var i = 0; i < 3 && columnsLength > 0;) {
                var insertion =
                    "<div class='section no-pad-bot'>" +
                    "<div class='container'>" +
                    "<div class='row'>";

                if (columnsLength < 4) {
                    max = columnsLength;
                    $('#watch-more-btn').hide();
                } else {
                    max = 4;
                }

                for (var j = 0; j < max; j++, projectCounter++) {
                    insertion +=
                        "<div class='col s12 m6 l3'>" +
                        "<div class='card'>" +
                        "<a href='/projects/update/" + result.content[projectCounter].id + "'>" +
                        "<div class='card-image'>" +
                        "<img src='" + result.content[projectCounter].imagePathName + "'>" +
                        "<span class='card-title'>" + result.content[projectCounter].projectName + "</span>" +
                        "</div>" +
                        "</a>" +
                        "<div class='card-content'>" +
                        "<a href='/projects/update/" + result.content[projectCounter].id + "' class='anchor-project-des'>" +
                        "<p>" + result.content[projectCounter].description + "</p>" +
                        "</a>" +
                        "</div>" +
                        "</div>" +
                        "</div>";
                }
                columnsLength = columnsLength - max;
                insertion +=
                    "</div>" +
                    $('.section:last')
                        .after(insertion);

            }
        }
    }
}

function registerHandler(callback) {
    var emailRegister = $('#email_register').val();

    var user = {
        emailAddress: emailRegister,
        userName: emailRegister.split('@')[0],
        name: $('#name_register').val(),
        password: $('#password_register').val()
    };

    $.ajax({
        url: '/register/submit',
        contentType: "application/json",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        type: 'post',
        data: JSON.stringify(user),
        success: function (result) {
            handleRegisterRequest(user, result);
        },
        error: handleRegisterError
    });

    if (typeof callback === 'function') {
        callback();
    }
}

function handleRegisterRequest(user, result) {
    if (
        result.userName == user.userName &&
        result.name == user.name &&
        result.emailAddress == user.emailAddress &&
        result.password == user.password
    ) {
        login(user.emailAddress, user.password);
    }
}

function handleRegisterError(err) {
    console.log('Register error');
    console.log(err);
}

function handleProjectCreation() {
    var project = {
        projectName: $('#project_name').val(),
        imagePathName: $('#project_image').val(),
        website: $('#project_website').val(),
        facebookSite: $('#facebook_site').val(),
        twitterSite: $('#twitter_site').val(),
        description: $('#description').val(),
        groupsNumber: $('#numero_grupos').val(),
        membersNumber: $('#numero_miembros').val()
    };

    if (
        project.projectName != null &&
        project.imagePathName != null &&
        project.description != null &&
        project.groupsNumber != null &&
        project.membersNumber != null
    ) {
        $.ajax({
            url: '/projects/create/submit',
            type: 'post',
            contentType: 'application/json',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(project),
            success: function (result) {
                handleProjectCreationRequest(project, result);
            },
            error: handleCreateProjectError
        });
    }
}

function handleProjectCreationRequest(project, result) {
    if (
        result.projectName == project.projectName &&
        result.description == project.description &&
        result.groupsNumber == project.groupsNumber &&
        result.membersNumber == project.membersNumber
    ) {
        window.location.replace("/projects");
    }
}

function handleCreateProjectError() {
    console.log("Error creating project.");
}

function handleGroupCreation() {

    var group = {
        name: $("#group_name").val(),
        description: $("#group_description").val()
    };

    if (
        group.name != null &&
        group.description != null
    ) {
        $.ajax({
            url: '/groups/create',
            type: 'post',
            contentType: 'application/json',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(group),
            success: function (result) {
                handleGroupCreationRequest(group, result);
            },
            error: handleCreateGroupError
        })
    }
}

function handleGroupCreationRequest(group, result) {
    if (group.name == result.name &&
        group.description == result.description
    ) {
        window.location.reload();
    }
}

function handleCreateGroupError() {
    console.log("Creation group error");
}

function handleGettingPublications(res) {

    if (res.content.length > 0) {

        for (var i = 0; i < res.content.length; i++) {
            $('.collection:first a:eq(' + i + ')')
                .text(res.content[i].title)
                .attr('href', '/publication/' + res.content[i].id)
        }

        $("i:contains('chevron_left')")
            .parent()
            .parent()
            .removeClass('disabled');

        firstPageBack = true;
    } else {
        pagePublicationsCounter--;

        $("i:contains('chevron_right')")
            .parent()
            .parent()
            .addClass('disabled');
    }
}

function handleGettingPublicationsError() {
    console.log("Error getting publications");
}

function handleGettingLessPublications(res) {

    if (res.content.length > 0) {

        for (var i = 0; i < res.content.length; i++) {
            $('.collection:first a:eq(' + i + ')')
                .text(res.content[i].title)
                .attr('href', '/publication/' + res.content[i].id)
        }

        if (pagePublicationsCounter == 0) {
            $("i:contains('chevron_left')")
                .parent()
                .parent()
                .addClass('disabled');

            $("i:contains('chevron_right')")
                .parent()
                .parent()
                .removeClass('disabled');
        }

    } else {
        pagePublicationsCounter++;

        $("i:contains('chevron_left')")
            .parent()
            .parent()
            .addClass('disabled');

        $("i:contains('chevron_right')")
            .parent()
            .parent()
            .removeClass('disabled');
    }
}

function handleGettingLessPublicationsError() {
    handleGettingPublicationsError();
}

function handleGettingNickname(res) {
    $("#searched-member")
        .show();

    if ($("#nickname-text").length == 0) {
        $("#searched-member div")
            .append("<span id='nickname-text'>" + res.nickname + "</span>");
    } else {
        $("#nickname-text")
            .empty()
            .text(res.nickname)
    }

    var splittedURI = window.location.href.split('/');

    if (!res.member) {
        $("#add-del-btn")
            .attr('data-badge-caption', 'Agregar');
    } else {
        $("#add-del-btn")
            .attr('data-badge-caption', 'Eliminar');
    }
}

function handleGettingNicknameError() {
    console.log("Error obtaining nickname");
    $("#searched-member").hide();
}